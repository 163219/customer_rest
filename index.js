/* the server */
var express = require("express");
var app = express();

var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({ layout:false }));
app.set("view engine", "handlebars");

var mysql = require("mysql");
const pool = mysql.createPool({
    host: "localhost",
    user: "fred",
    password: "fred",
    database: "derby_sample",
    connectionLimit: 3
});

var error = function(code, msg, res) {
    res.status(code);
    res.type("text/plain")
    res.end(msg);
}

//Setup routes
app.get("/customers", function(req, res) {
    pool.getConnection(function(err, conn) {
        if (err) {
            error(400, err, res);
            return;
        }
        try {
            conn.query("select CUSTOMER_ID,NAME,EMAIL from CUSTOMER", 
                function(err, rows) {
                    if (err) {
                        error(400, err, res);
                        return;
                    }
                    //res.status(200);
                    //res.type("application/json")
                    //res.send(rows);
                    console.info(">> request type: %s", req.headers["Content-Type"]);
                    res.format({
                        "text/html": function() {
                            res.render("customers", { customers: rows});
                        },
                        
                        "application/json": function() {
                            res.json(rows);
                        }
                    })
                });
        } catch (ex) {
            error(400, ex, res);
        } finally {
            conn.release();
        }
    });
});

app.get("/customer/:cust_id", function(req, res) {
    pool.getConnection(function(err, conn) {
        try {
            conn.query("select * from CUSTOMER where CUSTOMER_ID = ?",
                [req.params.cust_id], 
                function (err, rows) {
                    if (err) {
                        error(400, err);
                        return;
                    }
                    if (rows.length)
                        res.json(rows[0]);
                    else 
                        error(404, "Not found: " + req.params.cust_id, res);
                });
        } catch (ex) { } 
        finally {
            conn.release();
        }
    });
})

//If prefix is /bower_compoments look for it here
app.use("/bower_components", express.static(__dirname + "/bower_components"));

//Look for files in public regarless of their prefix
app.use(express.static(__dirname + "/public"));

//Send 404 since file is not available
app.use(function(req, res, next) {
    res.status(404);
    res.type("text/plain");
    res.send("File not found: " + req.originalUrl);
});

app.listen(3000, function() {
    console.info("Application started on port 3000");
});
