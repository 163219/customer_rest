/*
    Client side application
 */

(function() {
    
    var CustomerApp = angular.module("CustomerApp", ["ui.router"]);
    
    var ListCtrl = function($http, $state) {
        
        var vm = this;
        vm.customers = [];
        vm.gotoDetails = function(cid) {
            $state.go("details", 
                { cust_id: cid }
            )
        }
        
        $http.get("/customers")
            .then(function(result) {
                vm.customers = result.data;
            }).catch(function(err) {
                console.errro(">> error: %s", err);
            });
    }
    var DetailsCtrl = function($http, $stateParams, $state) {
        var vm = this;
        vm.customer = {};
        vm.goBack = function() {
            $state.go("list");
        };
        
        $http.get("/customer/" + $stateParams.cust_id)
            .then(function(result) {
                vm.customer = result.data
            }).catch(function(err) {
                console.error(">> error: %s", err);
            });
    }

    var CustomerConfig = function($stateProvider, $urlRouterProvider) {
        
        $stateProvider
            .state("list", {
                url: "/list", // #/list
                templateUrl: "/state/list.html",
                controller: ["$http", "$state", ListCtrl],
                controllerAs: "listCtrl"

            }).state("details", {
                url: "/customer/:cust_id",
                templateUrl: "/state/details.html",
                controller: ["$http", "$stateParams", "$state", DetailsCtrl],
                controllerAs: "detailsCtrl"
            });
        
        $urlRouterProvider.otherwise("/list");
    }
    var CustomerCtrl = function() {}

    CustomerApp.config(["$stateProvider", "$urlRouterProvider", CustomerConfig]);
    
    CustomerApp.controller("CustomerCtrl", [CustomerCtrl]);
    
})();












